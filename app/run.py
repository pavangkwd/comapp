from flask import Flask, render_template, request,session, redirect, url_for
from flaskext.mysql import MySQL
import json
import re
import hashlib
import requests

app = Flask(__name__)
mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'apnaarsenal'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)
salt = "thisisa$alt$houldNotchange"

app.secret_key = "clone_app"

@app.route("/")
def index():
	cursor = mysql.connect().cursor()
	cursor.execute("select links.link_id, links.url, links.description, links.upvotes, links.date_added ,users.username from users, links  where links.op_id = users.user_id order by links.date_added desc")
	res = cursor.fetchall()
	data = []
	for d in res:
		id_ = d[0]
		cursor = mysql.connect().cursor()
		cursor.execute("select count(*) from comments where link_id={0}".format(id_))
		comments = cursor.fetchone()
		data.append({"url":d[1],"desc" : d[2], "comments":int(comments[0]), "id":id_, "user":d[5]})
	return render_template("index.html", data = data)

@app.route("/submit/<item>", methods=["GET", "POST"])
def submit(item):
	if request.method == "POST":
		if item == "story":
			link = request.form.get("link")
			title = request.form.get("title")
			sql = "insert into links (description , url, op_id) values (%s, %s, %s)"
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sql, (title, link, session["user_id"]))
			conn.commit()
			return redirect(url_for("index"))
		if item == "comment":
			comment = request.form.get("comment")
			link_id = int(request.form.get("link_id"))
			sql = "insert into comments (comment, link_id, op_id) values (%s , {0} , {1})".format(link_id, session["user_id"])
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sql, (comment,))
			conn.commit()
			return redirect("/story/{0}".format(link_id))

	return render_template("submit.html")


@app.route("/story/<num>")
def story(num):
	query_links = "select links.url, links.description, users.username, users.username from links Inner Join users where links.link_id = {0} and links.op_id = users.user_id".format((num))
	query_comments = "select comments.comment , users.username from comments inner join users where comments.op_id = users.user_id and comments.link_id = {0} order by comments.date_added desc".format(int(num))
	cursor = mysql.connect().cursor()
	cursor.execute(query_links)
	links = cursor.fetchone()
	cursor.execute(query_comments)
	comments = cursor.fetchall()
	url = links[0]
	user = links[2]
	description = links[1]
	com = []
	for c in comments:
		com.append((c[0], c[1],))
	print com
	return render_template("story.html", story={"url":url, "description":description, "link_id":int(num), "user":user}, comments=com)

@app.route("/guidelines")
def guidelines():
	return render_template("guidelines.html")

@app.route("/login", methods=["POST","GET"])
def login():
	if request.method == "POST":
		username = request.form.get("username")
		password = request.form.get("password")
		password = hashlib.sha512(password + salt).hexdigest()
		if request.form.get("type") == "register":
			sql = "insert into users (username, password) values (%s, %s)"
			print sql
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sql, (username, password))
			conn.commit()

			sql = "select * from users where username=%s"
			cursor = mysql.connect().cursor()
			cursor.execute(sql, (username,))
			res = cursor.fetchone()

			session["username"] = username
			session["user_id"] = int(res[0])
			return redirect("/user/{0}".format(username))
		elif request.form.get("type") == "login":
			sql = "select * from users where username=%s and password=%s"
			cursor = mysql.connect().cursor()
			cursor.execute(sql, (username, password))
			res = cursor.fetchone()
			print res
			if res:
				session["username"] = username
				session["user_id"] = int(res[0])
				return redirect("/user/{0}".format(username))

		return render_template("register.html")
	if request.method == "GET":
		return render_template("register.html")


@app.route("/user/<username>")
def profile(username):
	sql = "select fan_score from users where username='{0}'".format(username)
	cursor = mysql.connect().cursor()
	cursor.execute(sql)
	data = cursor.fetchone()
	return render_template("profile.html", data={"username":username, "own":False, "fan_score":int(data[0])})

@app.route("/fixtures")
def fixtures():
	sql = "select * from arsenal_fixtures"
	cursor = mysql.connect().cursor()
	cursor.execute(sql)
	res = cursor.fetchall()
	return render_template("fixtures.html", data=res)

@app.route("/logout")
def logout():
	session.clear()
	return redirect(url_for("index"))

@app.route("/update")
def update():
	url = "http://api.football-data.org/alpha/teams/57/fixtures"
	data=json.loads(requests.get(url, headers={'X-Auth-Token':'d02139381a4e4b8b81fee6108d74d5a5'}).text)
	# fixtures = []
	for fix in data["fixtures"]:
		team = ""

		if fix["homeTeamName"] != "FC Arsenal London":	
			team = fix["homeTeamName"]
		else:
			team = fix["awayTeamName"]

		venue = "Home" if fix["homeTeamName"] == "FC Arsenal London" else "Away"
		score = [-1, -1]
		if fix["status"] == "FINISHED":
			if venue == "Home":
				score  = [fix["result"]["goalsHomeTeam"], fix["result"]["goalsAwayTeam"]]
			else:
				score  = [fix["result"]["goalsAwayTeam"], fix["result"]["goalsHomeTeam"]]
		league = json.loads(requests.get(fix["_links"]["soccerseason"]["href"],headers={'X-Auth-Token':'d02139381a4e4b8b81fee6108d74d5a5'}).text)["caption"]
		date = fix["date"].split("T")[0]
		sql = "insert into arsenal_fixtures (team, venue, ars_goals, opp_goals, league, date) values (%s,%s,%s, %s,%s,%s)"
		conn = mysql.connect()
		cursor = conn.cursor()
		cursor.execute(sql, (team, venue, int(score[0]), int(score[1]), league, date))
		conn.commit()
		# fixtures.append({"team": team, "venue":venue, "score":score,"league":league, "date" : date })
	return "Done!"




def build_insert(table, data):
	sql = "insert into {0} (".format(table)
	for key in data.keys():
		sql += key + ","
	sql = sql[:-1] + ") values ("
	for val in data.values():
		
		if type(val) == type("") or type(val) == type(u""):
			val = json.dumps(val)
			sql += "'{0}',".format(val)	
		else:
			sql += str(val) + ","
	sql = sql[:-1] + ")"
	return sql


if __name__ == '__main__':
	app.run(debug=True)
'''
tables :

users 
user_id
username
password
email
fan_score
date_added

create table users (
	user_id int NOT NULL AUTO_INCREMENT,
	username varchar(30) NOT NULL UNIQUE,
	password varchar(1024) NOT NULL,
	email varchar(100),
	date_added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	fan_score int DEFAULT 10,
	PRIMARY KEY (user_id)
	);

links 
link_id
url
description
upvotes
date_added
op_id

create table links (
	link_id int NOT NULL AUTO_INCREMENT,
	url varchar(1024) NOT NULL,
	description TEXT NOT NULL,
	upvotes int NOT NULL DEFAULT 0,
	op_id int NOT NULL,
	date_added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (link_id) ,
	FOREIGN KEY (op_id) REFERENCES users(user_id)
	ON UPDATE CASCADE ON DELETE RESTRICT
	);

comments
comment_id
link_id
op_id
upvotes
comment
date_added
parent_id

create table comments (
	comment_id int NOT NULL AUTO_INCREMENT,
	link_id int NOT NULL,
	op_id int NOT NULL,
	comment varchar(1024) NOT NULL,
	upvotes int NOT NULL DEFAULT 0,
	parent_id int DEFAULT 0,
	date_added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (link_id) REFERENCES links(link_id)
	ON UPDATE CASCADE,
	FOREIGN KEY (op_id) REFERENCES users(user_id)
	ON UPDATE CASCADE,
	PRIMARY KEY (comment_id)
	);

arsenal_fixtures
team, venue, date, status, goals_home , goals_away, league, 

create table arsenal_fixtures (
	fixture_id int NOT NULL AUTO_INCREMENT,
	team varchar(200) NOT NULL,
	venue varchar(10) NOT NULL,
	date date NOT NULL,
	ars_goals int ,
	opp_goals int,
	league varchar(100),
	PRIMARY KEY (fixture_id)
	);

'''
